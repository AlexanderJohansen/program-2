package com.apps.app.p_tracks;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander on 6/15/2016.
 */
public class UserDBHandler extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 2;
    // Database Name
    private static final String DATABASE_NAME = "userInfo";
    // Contacts table name
    private static final String TABLE_USER = "user";
    // User Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_USER_ID = "_id";
    private static final String KEY_COMPANY = "company";
    private static final String KEY_CARDID = "cardid";
    private static final String KEY_ISACTIVE = "isactive";
    private static final String KEY_JOBTITLE = "jobtitle";
    private static final String KEY_ESTATUS = "estatus";
    private static final String KEY_FIRSTNAME = "firstname";
    private static final String KEY_LASTNAME = "lastname";
    private static final String KEY_CREATED = "created";

    public UserDBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_USER + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_USER_ID + " TEXT,"
                + KEY_COMPANY + " TEXT," + KEY_CARDID + " TEXT," + KEY_ISACTIVE + " INTEGER DEFAULT 0," + KEY_JOBTITLE + " TEXT,"
                + KEY_ESTATUS + " TEXT," + KEY_FIRSTNAME + " TEXT,"
                + KEY_LASTNAME + " TEXT," + KEY_CREATED + " INTEGER DEFAULT 0" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        // Creating tables again
        onCreate(db);
    }

    // Adding new User
    public void addUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_USER_ID, user.get_id()); // User _id
        values.put(KEY_COMPANY, user.getCompany()); // User Company
        values.put(KEY_CARDID, user.getCardid()); // User CardID
        values.put(KEY_ISACTIVE, user.getIsactive());
        values.put(KEY_JOBTITLE, user.getJobtitle());
        values.put(KEY_ESTATUS, user.getEstatus());
        values.put(KEY_FIRSTNAME, user.getFirstname());
        values.put(KEY_LASTNAME, user.getLastname());
        values.put(KEY_CREATED, user.getCreated());
        //
        db.insert(TABLE_USER, null, values);
        db.close(); // Closing database connection
    }

    // Getting one User
    public User getUser(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_USER, new String[] { KEY_ID,
                        KEY_USER_ID, KEY_COMPANY, KEY_CARDID, KEY_ISACTIVE,
                        KEY_JOBTITLE, KEY_ESTATUS, KEY_FIRSTNAME, KEY_LASTNAME, KEY_CREATED }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        User contact = new User(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3),
                Integer.parseInt(cursor.getString(4)), cursor.getString(5), cursor.getString(6),
                cursor.getString(7), cursor.getString(8), Integer.parseInt(cursor.getString(9)));
        // return user
        return contact;
    }

    // Getting All Users
    public List<User> getAllUsers() {
        List<User> shopList = new ArrayList<User>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_USER;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                User user = new User();
                user.setId(Integer.parseInt(cursor.getString(0)));
                user.set_id(cursor.getString(1));
                user.setCompany(cursor.getString(2));
                user.setCardid(cursor.getString(3));
                user.setIsactive(Integer.parseInt(cursor.getString(4)));
                user.setJobtitle(cursor.getString(5));
                user.setEstatus(cursor.getString(6));
                user.setFirstname(cursor.getString(7));
                user.setLastname(cursor.getString(8));
                user.setCreated(Integer.parseInt(cursor.getString(9)));
                // Adding contact to list
                shopList.add(user);
            } while (cursor.moveToNext());
        }
        // return contact list
        return shopList;
    }

    // Getting Users Count
    public int getUsersCount() {
        String countQuery = "SELECT * FROM " + TABLE_USER;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();
        // return count
        return cursor.getCount();
    }

    // Updating a User
    public int updateUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_USER_ID, user.get_id());
        values.put(KEY_COMPANY, user.getCompany());
        values.put(KEY_CARDID, user.getCardid());
        values.put(KEY_ISACTIVE, user.getIsactive());
        values.put(KEY_JOBTITLE, user.getJobtitle());
        values.put(KEY_ESTATUS, user.getEstatus());
        values.put(KEY_FIRSTNAME, user.getFirstname());
        values.put(KEY_LASTNAME, user.getLastname());
        values.put(KEY_CREATED, user.getCreated());
        // updating row
        return db.update(TABLE_USER, values, KEY_ID + " = ?",
                new String[]{String.valueOf(user.getId())});
    }

    // Deleting a User
    public void deleteUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_USER, KEY_ID + " = ?",
                new String[] { String.valueOf(user.getId()) });
        db.close();
    }
}
