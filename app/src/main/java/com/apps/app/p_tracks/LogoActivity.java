package com.apps.app.p_tracks;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.os.StrictMode;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.List;


public class LogoActivity extends AppCompatActivity {

    private final int SPLASH_DISPLAY_LENGTH = 5000;

    private static final String TAG = "Contacts";

    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;

    String UserID;
    Integer UserIsActive;

    StringBuilder sb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_logo);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        try{
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                UserDBHandler db = new UserDBHandler(this);
                Log.d("Reading: ", "Reading all users..");
                List<User> users = db.getAllUsers();
                int users_number = users.size();

                if (users_number > 0) {
                    for (User user : users) {
                        String log = "Id: " + user.getId() + " ,UserID: " + user.get_id()
                                + " ,Company: " + user.getCompany() + " ,CardID: " + user.getCardid()
                                + " ,IsActive: " + user.getIsactive() + " ,JobTitle: " + user.getJobtitle()
                                + " ,EStatus: " + user.getEstatus() + " ,FirstName: " + user.getFirstname()
                                + " ,LastName: " + user.getLastname() + " ,Created: " + user.getCreated();
                        Log.d("User: : ", log);
                    }
                    User lastUser = new User();
                    lastUser = db.getUser(users_number);
                    UserID = lastUser.get_id();
                    UserIsActive = lastUser.getIsactive();

                    if (UserIsActive == 1) {//if isactive = true, then show the scan page,
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent mainIntent = new Intent(LogoActivity.this, ScanActivity.class);
                                LogoActivity.this.startActivity(mainIntent);
                                LogoActivity.this.finish();
                            }
                        }, SPLASH_DISPLAY_LENGTH);
                    } else {
                        Toast.makeText(getApplicationContext(), "ISACTIVE is false. Please Update your profile", Toast.LENGTH_LONG).show();
                    }
                } else {
                    insertDummyContactWrapper();
                }

            }else{
                UserDBHandler db = new UserDBHandler(this);
                Log.d("Reading: ", "Reading all users..");
                List<User> users = db.getAllUsers();
                int users_number = users.size();

                if (users_number > 0){
                    for (User user : users) {
                        String log = "Id: " + user.getId() + " ,UserID: " + user.get_id()
                                + " ,Company: " + user.getCompany() + " ,CardID: " + user.getCardid()
                                + " ,IsActive: " + user.getIsactive() + " ,JobTitle: " + user.getJobtitle()
                                + " ,EStatus: " + user.getEstatus() + " ,FirstName: " + user.getFirstname()
                                + " ,LastName: " + user.getLastname() + " ,Created: " + user.getCreated();
                        Log.d("User: : ", log);
                    }
                    User lastUser = new User();
                    lastUser = db.getUser(users_number);
                    UserID = lastUser.get_id();
                    UserIsActive = lastUser.getIsactive();

                    if(UserIsActive == 1){
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent mainIntent = new Intent(LogoActivity.this, ScanActivity.class);
                                LogoActivity.this.startActivity(mainIntent);
                                LogoActivity.this.finish();
                            }
                        }, SPLASH_DISPLAY_LENGTH);
                    }else{
                        Toast.makeText(getApplicationContext(),"ISACTIVE is false. Please Update your profile",Toast.LENGTH_LONG).show();
                    }
                }else{
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent mainIntent = new Intent(LogoActivity.this, RegisterActivity.class);
                            mainIntent.putExtra("user_state","initial_state");
                            LogoActivity.this.startActivity(mainIntent);
                            finish();
                        }
                    }, SPLASH_DISPLAY_LENGTH);
                }

            }
        }catch (Exception e){
            Log.e(TAG, e.toString());
            e.printStackTrace();
        }

    }

    @TargetApi(Build.VERSION_CODES.M)
    private void insertDummyContactWrapper() {
        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write External Storage");
        if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE))
            permissionsNeeded.add("Read External Storage");

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                showMessageOKCancel(message,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                                UserDBHandler db = new UserDBHandler(LogoActivity.this);
                                // Reading all Users
                                Log.d("Reading: ", "Reading all users..");
                                List<User> users = db.getAllUsers();
                                int users_number = users.size();

                                if (users_number > 0){
                                    for (User user : users) {
                                        String log = "Id: " + user.getId() + " ,UserID: " + user.get_id()
                                                + " ,Company: " + user.getCompany() + " ,CardID: " + user.getCardid()
                                                + " ,IsActive: " + user.getIsactive() + " ,JobTitle: " + user.getJobtitle()
                                                + " ,EStatus: " + user.getEstatus() + " ,FirstName: " + user.getFirstname()
                                                + " ,LastName: " + user.getLastname() + " ,Created: " + user.getCreated();
                                        Log.d("User: : ", log);
                                    }
                                    User lastUser = new User();
                                    lastUser = db.getUser(users_number);
                                    UserID = lastUser.get_id();
                                    UserIsActive = lastUser.getIsactive();

                                    if(UserIsActive == 1){
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                Intent mainIntent = new Intent(LogoActivity.this, ScanActivity.class);
                                                LogoActivity.this.startActivity(mainIntent);
                                                LogoActivity.this.finish();
                                            }
                                        }, SPLASH_DISPLAY_LENGTH);
                                    }else{
                                        Toast.makeText(getApplicationContext(),"ISACTIVE is false. Please Update your profile",Toast.LENGTH_LONG).show();
                                    }
                                }else{
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            Intent mainIntent = new Intent(LogoActivity.this, RegisterActivity.class);
                                            mainIntent.putExtra("user_state","initial_state");
                                            LogoActivity.this.startActivity(mainIntent);
                                            finish();
                                        }
                                    }, SPLASH_DISPLAY_LENGTH);
                                }
                            }
                        });
                return;
            }
            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            return;
        }

        insertDummyContact();
    }
    @TargetApi(Build.VERSION_CODES.M)
    private void insertDummyContact() {
        ArrayList<ContentProviderOperation> operations = new ArrayList<ContentProviderOperation>(2);
        ContentProviderOperation.Builder op =
                ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                        .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                        .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null);
        operations.add(op.build());
        op = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                .withValue(ContactsContract.Data.MIMETYPE,
                        ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME,
                        "__DUMMY CONTACT from runtime permissions sample");
        operations.add(op.build());
        ContentResolver resolver = getContentResolver();
        try {
            resolver.applyBatch(ContactsContract.AUTHORITY, operations);
        } catch (RemoteException e) {
            Log.d(TAG, "Could not add a new contact: " + e.getMessage());
        } catch (OperationApplicationException e) {
            Log.d(TAG, "Could not add a new contact: " + e.getMessage());
        }
    }
    @TargetApi(Build.VERSION_CODES.M)
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!shouldShowRequestPermissionRationale(permission))
                return false;
        }
        return true;
    }
    @TargetApi(Build.VERSION_CODES.M)
    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(LogoActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }
}
