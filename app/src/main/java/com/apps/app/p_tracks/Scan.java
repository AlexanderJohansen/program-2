package com.apps.app.p_tracks;

/**
 * Created by Alexander on 6/16/2016.
 */
public class Scan {

    private int id;
    private String visitid;
    private String userid;
    private String locationid;
    private String location;
    private String currenttime;
    private String timestamp;
    private String type;
    private String SyncDatetime;
    private String ServerID;

    public Scan(){

    }

    public Scan(int id, String visitid, String userid, String locationid, String location,String currenttime, String timestamp, String type, String SyncDatetime, String ServerID){
        this.id = id;
        this.visitid = visitid;
        this.userid = userid;
        this.locationid = locationid;
        this.location = location;
        this.currenttime = currenttime;
        this.timestamp = timestamp;
        this.type = type;
        this.SyncDatetime = SyncDatetime;
        this.ServerID = ServerID;
    }

    public void setId(int id){ this.id = id; }

    public void setVisitid(String visitid){ this.visitid = visitid; }

    public void setUserid(String userid){ this.userid = userid; }

    public void setLocationid(String locationid){ this.locationid = locationid; }

    public void setLocation(String location){ this.location = location; }

    public void setCurrenttime(String currenttime){ this.currenttime = currenttime; }

    public void setTimestamp(String timestamp){ this.timestamp = timestamp; }

    public void setType(String type){ this.type = type; }

    public void setSyncDatetime(String syncDatetime){ this.SyncDatetime = syncDatetime; }

    public void setServerID(String serverID){ this.ServerID = serverID; }

    public int getId(){ return id; }

    public String getVisitid(){
        return visitid;
    }

    public String getUserid(){
        return userid;
    }

    public String getLocationid(){
        return locationid;
    }

    public String getLocation(){
        return location;
    }

    public String getCurrenttime(){
        return currenttime;
    }

    public String getTimestamp(){
        return timestamp;
    }

    public String getType(){
        return type;
    }

    public String getSyncDatetime(){
        return SyncDatetime;
    }

    public String getServerID(){
        return ServerID;
    }
}
