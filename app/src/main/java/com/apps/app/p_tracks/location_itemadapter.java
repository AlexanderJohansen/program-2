package com.apps.app.p_tracks;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by Alexander on 6/17/2016.
 */
public class location_itemadapter extends BaseAdapter {
    Context context;
    private static LayoutInflater inflater=null;
    String [] time_array, location_array, type_array;

    public location_itemadapter(ScanLogActivity scanLogActivity, String[] time_array, String[] location_array, String[] type_array) {
        // TODO Auto-generated constructor stub

        this.context = scanLogActivity;
        this.time_array = time_array;
        this.location_array = location_array;
        this.type_array = type_array;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return time_array.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView time_view, location_view, type_view;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.location_detailitem, null);
        holder.time_view=(TextView) rowView.findViewById(R.id.time_array);
        holder.location_view=(TextView) rowView.findViewById(R.id.location_array);
        holder.type_view=(TextView) rowView.findViewById(R.id.type_array);
        holder.time_view.setText(time_array[position]);
        holder.location_view.setText(location_array[position]);

        if(type_array[position] == null) {
        }else{
            if(type_array[position].equals("enter")){
                holder.type_view.setText("IN");
                holder.type_view.setBackgroundColor(Color.parseColor("#00ff00"));
            }else{
                holder.type_view.setText("OUT");
                holder.type_view.setBackgroundColor(Color.parseColor("#ff0000"));
            }
        }

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
            }
        });
        return rowView;
    }


}
