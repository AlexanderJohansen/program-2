package com.apps.app.p_tracks;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.Button;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity {

    Button btnRegister;

    EditText firstName, lastName, company, jobTitle, cardID;
    Spinner employmentStatus;

    String fn, ln, co, ci, jt, es;

    StringBuilder sb;

    //User ID
    String UserID;

    private ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("P-TRACKS");
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.nav_scanlog) {
                } else if (id == R.id.nav_profile) {
                } else if (id == R.id.nav_feedback) {
                    // Handle the feedback action
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(RegisterActivity.this);
                    alertDialog.setTitle("Feedback");
                    alertDialog.setMessage("Enter Feedback");

                    final EditText input = new EditText(RegisterActivity.this);
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT);
                    input.setLayoutParams(lp);
                    alertDialog.setView(input);
                    alertDialog.setIcon(R.drawable.feedback);

                    alertDialog.setPositiveButton("YES",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    String feedback;
                                    feedback = input.getText().toString();
                                    Toast.makeText(getApplicationContext(),
                                            feedback, Toast.LENGTH_SHORT).show();
                                }
                            });

                    alertDialog.setNegativeButton("NO",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });

                    alertDialog.show();
                } else if (id == R.id.nav_quit) {
                    // Handle the quit action
                    RegisterActivity.this.finish();
                }

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });


        btnRegister = (Button) findViewById(R.id.Register);

        firstName = (EditText) findViewById(R.id.firstName);
        lastName = (EditText) findViewById(R.id.lastName);
        company = (EditText) findViewById(R.id.company);
        cardID = (EditText) findViewById(R.id.cardID);
        jobTitle = (EditText) findViewById(R.id.jobTitle);
        employmentStatus = (Spinner) findViewById(R.id.employmentStatus);

        this.get_currentUserData();

        Intent intent = getIntent();
        String user_state;
        user_state = intent.getStringExtra("user_state");
        if(user_state.equals("initial_state")){
            btnRegister.setText("Register");
            btnRegister.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Get user defined values
                    fn = firstName.getText().toString();
                    ln = lastName.getText().toString();
                    co = company.getText().toString();
                    ci = cardID.getText().toString();
                    jt = jobTitle.getText().toString();
                    es = employmentStatus.getSelectedItem().toString();
                    progress = new ProgressDialog(RegisterActivity.this);
                    progress.setMessage("Registering User ...");
                    progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progress.show();
                    AsyncT asyncT = new AsyncT();
                    asyncT.execute();
                }
            });
        }else {
            btnRegister.setText("Update");
            btnRegister.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Get user defined values
                    fn = firstName.getText().toString();
                    ln = lastName.getText().toString();
                    co = company.getText().toString();
                    ci = cardID.getText().toString();
                    jt = jobTitle.getText().toString();
                    es = employmentStatus.getSelectedItem().toString();
                    progress = new ProgressDialog(RegisterActivity.this);
                    progress.setMessage("Updating User ...");
                    progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progress.show();
                    AsyncT_update asyncT_update = new AsyncT_update();
                    asyncT_update.execute();
                }
            });
        }


    }

    private class AsyncT extends AsyncTask<String , String, String> {

        protected String doInBackground(String... params) {
            try {
                URL url = new URL("http://ptracks.com/api/users");

                HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                connection.setDoOutput(true);
                connection.setDoInput(true);
                connection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
                connection.setRequestMethod("POST");

                String deviceid, os, version;
                deviceid = android.os.Build.DEVICE;
                os = "android";
                version = System.getProperty("os.version");

                JSONObject jsonobj = new JSONObject();
                JSONObject deviceInfo = new JSONObject();
                JSONObject payloadInfo = new JSONObject();
                payloadInfo.put("os", os);
                payloadInfo.put("version", version);
                deviceInfo.put("id", deviceid);
                deviceInfo.put("payload", payloadInfo);
                jsonobj.put("firstname", fn);
                jsonobj.put("lastname", ln);
                jsonobj.put("company", co);
                jsonobj.put("cardid", ci);
                jsonobj.put("estatus", es);
                jsonobj.put("device", deviceInfo);
                jsonobj.put("jobtitle", jt);

                OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
                wr.write(jsonobj.toString());
                wr.flush();

                //display what returns the POST request

                sb = new StringBuilder();
                int HttpResult = connection.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(connection.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();
                    System.out.println("" + sb.toString());
                } else {
                    System.out.println(connection.getResponseMessage());
                }
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());
            }
            return sb.toString();
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {

                JSONObject object = new JSONObject(result);
                Log.d("My App", object.toString());

                Integer __v = object.getInt("__v");
                String company = object.getString("company");
                String cardid = object.getString("cardid");
                String _id = object.getString("_id");
                Boolean isactive = object.getBoolean("isactive");
                String jobtitle = object.getString("jobtitle");
                String estatus = object.getString("estatus");
                String firstname = object.getString("firstname");
                String lastname = object.getString("lastname");
                Boolean created = object.getBoolean("created");

                JSONArray json_array = object.getJSONArray("device");
                for(int j = 0;j < json_array.length();j ++){
                    Integer registered = json_array.getJSONObject(j).getInt("registered");
                    Integer lastactive = json_array.getJSONObject(j).getInt("lastactive");
                    Boolean isactive1 = json_array.getJSONObject(j).getBoolean("isactive");

                    String id = json_array.getJSONObject(j).getString("id");
                }

                Integer isactive_change, created_change;
                if(isactive == true){
                    isactive_change = 1;
                }else{
                    isactive_change = 0;
                }
                if(created == true){
                    created_change = 1;
                }else {
                    created_change = 0;
                }

                UserDBHandler db = new UserDBHandler(RegisterActivity.this);
                // Inserting User/Rows
                Log.d("Insert: ", "Inserting ..");
                db.addUser(new User(__v ,_id, company, cardid, isactive_change, jobtitle, estatus, firstname, lastname, created_change));
                // Reading all Users
                Log.d("Reading: ", "Reading all users..");
                List<User> users = db.getAllUsers();

                for (User user : users) {
                    String log = "Id: " + user.getId() + " ,UserID: " + user.get_id() + " ,Company: " + user.getCompany() + " ,CardID: " + user.getCardid()
                            + " ,IsActive: " + user.getIsactive() + " ,JobTitle: " + user.getJobtitle() + " ,EStatus: " + user.getEstatus()
                            + " ,FirstName: " + user.getFirstname() + " ,LastName: " + user.getLastname() + " ,Created: " + user.getCreated();
                    // Writing Users to log
                    Log.d("User: : ", log);
                }

                //last user finding
                User lastUser = new User();
                Integer last = users.size();
                lastUser = db.getUser(last);
                UserID = lastUser.get_id();

                if(isactive == true){
                    progress.dismiss();
                    Intent intent = new Intent(RegisterActivity.this, RegisterOKActivity.class);
                    intent.putExtra("UserID", UserID);
                    startActivity(intent);
                    RegisterActivity.this.finish();
                }
            } catch (Throwable t) {
                Log.e("My App", "Could not parse malformed JSON: \"" + result + "\"");
            }
        }
    }

    private class AsyncT_update extends AsyncTask<String , String, String> {

        protected String doInBackground(String... params) {
            try {
                URL url = new URL("http://ptracks.com/api/users");

                HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                connection.setDoOutput(true);
                connection.setDoInput(true);
                connection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
                connection.setRequestMethod("POST");

                String deviceid, os, version;
                deviceid = android.os.Build.DEVICE;
                os = "android";
                version = System.getProperty("os.version");

                JSONObject jsonobj = new JSONObject();
                JSONObject deviceInfo = new JSONObject();
                JSONObject payloadInfo = new JSONObject();
                payloadInfo.put("os", os);
                payloadInfo.put("version", version);
                deviceInfo.put("id", deviceid);
                deviceInfo.put("payload", payloadInfo);
                jsonobj.put("firstname", fn);
                jsonobj.put("lastname", ln);
                jsonobj.put("company", co);
                jsonobj.put("cardid", ci);
                jsonobj.put("estatus", es);
                jsonobj.put("device", deviceInfo);
                jsonobj.put("jobtitle", jt);

                OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
                wr.write(jsonobj.toString());
                wr.flush();

                //display what returns the POST request

                sb = new StringBuilder();
                int HttpResult = connection.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(connection.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();
                    System.out.println("" + sb.toString());
                } else {
                    System.out.println(connection.getResponseMessage());
                }
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());
            }
            return sb.toString();
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {

                JSONObject object = new JSONObject(result);
                Log.d("My App", object.toString());

                Integer __v = object.getInt("__v");
                String company = object.getString("company");
                String cardid = object.getString("cardid");
                String _id = object.getString("_id");
                Boolean isactive = object.getBoolean("isactive");
                String jobtitle = object.getString("jobtitle");
                String estatus = object.getString("estatus");
                String firstname = object.getString("firstname");
                String lastname = object.getString("lastname");
                Boolean created = object.getBoolean("created");

                JSONArray json_array = object.getJSONArray("device");
                for(int j = 0;j < json_array.length();j ++){
                    Integer registered = json_array.getJSONObject(j).getInt("registered");
                    Integer lastactive = json_array.getJSONObject(j).getInt("lastactive");
                    Boolean isactive1 = json_array.getJSONObject(j).getBoolean("isactive");

                    String id = json_array.getJSONObject(j).getString("id");
                }

                Integer isactive_change, created_change;
                if(isactive == true){
                    isactive_change = 1;
                }else{
                    isactive_change = 0;
                }
                if(created == true){
                    created_change = 1;
                }else {
                    created_change = 0;
                }

                UserDBHandler db = new UserDBHandler(RegisterActivity.this);
                // Inserting User/Rows
                Log.d("Insert: ", "Inserting ..");
                db.addUser(new User(__v ,_id, company, cardid, isactive_change, jobtitle, estatus, firstname, lastname, created_change));
                // Reading all Users
                Log.d("Reading: ", "Reading all users..");
                List<User> users = db.getAllUsers();

                for (User user : users) {
                    String log = "Id: " + user.getId() + " ,UserID: " + user.get_id() + " ,Company: " + user.getCompany() + " ,CardID: " + user.getCardid()
                            + " ,IsActive: " + user.getIsactive() + " ,JobTitle: " + user.getJobtitle() + " ,EStatus: " + user.getEstatus()
                            + " ,FirstName: " + user.getFirstname() + " ,LastName: " + user.getLastname() + " ,Created: " + user.getCreated();
                    // Writing Users to log
                    Log.d("User: : ", log);
                }

                //last user finding
                User lastUser = new User();
                Integer last = users.size();
                lastUser = db.getUser(last);
                UserID = lastUser.get_id();

                if(isactive == true){
                    progress.dismiss();
                    Intent intent = new Intent(RegisterActivity.this, RegisterOKActivity.class);
                    intent.putExtra("UserID", UserID);
                    startActivity(intent);
                    RegisterActivity.this.finish();
                }
            } catch (Throwable t) {
                Log.e("My App", "Could not parse malformed JSON: \"" + result + "\"");
            }
        }
    }

    void get_currentUserData(){
        UserDBHandler db = new UserDBHandler(RegisterActivity.this);
        Log.d("Reading: ", "Reading all users..");
        List<User> users = db.getAllUsers();
        int users_number = users.size();
        if(users_number > 0){
            for (User user : users) {
                String log = "Id: " + user.getId() + " ,UserID: " + user.get_id() + " ,Company: " + user.getCompany() + " ,CardID: " + user.getCardid();
                // Writing Users to log
                Log.d("User: : ", log);
            }
            User lastUser = new User();
            Integer last = users.size();
            lastUser = db.getUser(last);
            UserID = lastUser.get_id();
            firstName.setText(lastUser.getFirstname());
            lastName.setText(lastUser.getLastname());
            company.setText(lastUser.getCompany());
            jobTitle.setText(lastUser.getJobtitle());
            cardID.setText(lastUser.getCardid());
        }
    }
}
