package com.apps.app.p_tracks;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.hardware.Camera;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class QR_BarcodeScanActivity extends AppCompatActivity {

    private Camera mCamera;
    private CameraPreview mPreview;
    private Handler autoFocusHandler;

    private Button scanButton;
    private ImageScanner scanner;
    ImageButton btnMenu;

    private boolean barcodeScanned = false;
    private boolean previewing = true;

    // Getting LocationID variable
    String LocationID;
    String LocationName = "";
    String LocationName1, Unitname, SiteName, CountyName, StateName, local_datetime;
    // Getting UserID variable
    String UserID;
    // Getting Current time & Timestamp variable
    String Current_Time;
    Long tsLong;
    String Current_Timestamp;
    // Getting Type variable
    String Type = "", current_Type;
    // Getting visit ID
    String visitID;
    // Gettting SyncDatetime
    int SyncDateTime;
    // Getting ServerID
    String ServerID;

    StringBuilder sb;

    static {
        System.loadLibrary("iconv");
    }

    String responseServer;

    Integer __v, timestamp;
    String userid,locationid, type, _id;
    long synctimestamp;
    String visitID_received, Serverid, Synctimestamp;

    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr__barcode_scan);

        initControls();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Scan QRcode");
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.nav_scanlog) {
                    releaseCamera();
                    // Handle the scanlog  action
                    Intent intent = new Intent(QR_BarcodeScanActivity.this, ScanLogActivity.class);
                    startActivity(intent);
                    QR_BarcodeScanActivity.this.finish();
                } else if (id == R.id.nav_profile) {
                    releaseCamera();
                    // Handle the profile action
                    Intent intent = new Intent(QR_BarcodeScanActivity.this, RegisterActivity.class);
                    intent.putExtra("user_state","update_state");
                    startActivity(intent);
                    QR_BarcodeScanActivity.this.finish();
                } else if (id == R.id.nav_feedback) {
                    // Handle the feedback action
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(QR_BarcodeScanActivity.this);
                    alertDialog.setTitle("Feedback");
                    alertDialog.setMessage("Enter Feedback");

                    final EditText input = new EditText(QR_BarcodeScanActivity.this);
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT);
                    input.setLayoutParams(lp);
                    alertDialog.setView(input);
                    alertDialog.setIcon(R.drawable.feedback);

                    alertDialog.setPositiveButton("YES",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    String feedback;
                                    feedback = input.getText().toString();
                                    Toast.makeText(getApplicationContext(),
                                            feedback, Toast.LENGTH_SHORT).show();
                                }
                            });

                    alertDialog.setNegativeButton("NO",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });

                    alertDialog.show();
                }
                else if(id==R.id.nav_quit)
                {
                    // Handle the quit action
                    QR_BarcodeScanActivity.this.finish();
                }

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });

    }

    private void initControls() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        autoFocusHandler = new Handler();
        mCamera = getCameraInstance();

        // Instance barcode scanner
        scanner = new ImageScanner();
        scanner.setConfig(0, Config.X_DENSITY, 3);
        scanner.setConfig(0, Config.Y_DENSITY, 3);

        mPreview = new CameraPreview(QR_BarcodeScanActivity.this, mCamera, previewCb,
                autoFocusCB);
        FrameLayout preview = (FrameLayout) findViewById(R.id.cameraPreview);
        preview.addView(mPreview);

        scanButton = (Button) findViewById(R.id.ScanButton);

        scanButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (barcodeScanned) {
                    barcodeScanned = false;
                    mCamera.setPreviewCallback(previewCb);
                    mCamera.startPreview();
                    previewing = true;
                    mCamera.autoFocus(autoFocusCB);
                }
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            releaseCamera();
        }
        return super.onKeyDown(keyCode, event);
    }

    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e) {
        }
        return c;
    }

    private void releaseCamera() {
        if (mCamera != null) {
            previewing = false;
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
    }

    private Runnable doAutoFocus = new Runnable() {
        public void run() {
            if (previewing)
                mCamera.autoFocus(autoFocusCB);
        }
    };

    Camera.PreviewCallback previewCb = new Camera.PreviewCallback() {
        public void onPreviewFrame(byte[] data, Camera camera) {
            Camera.Parameters parameters = camera.getParameters();
            Camera.Size size = parameters.getPreviewSize();

            Image barcode = new Image(size.width, size.height, "Y800");
            barcode.setData(data);

            int result = scanner.scanImage(barcode);

            if (result != 0) {
                previewing = false;
                mCamera.setPreviewCallback(null);
                mCamera.stopPreview();

                SymbolSet syms = scanner.getResults();
                for (Symbol sym : syms) {

                    Log.i("<<<<<<Asset Code>>>>> ",
                            "<<<<Bar Code>>> " + sym.getData());
                    String scanResult = sym.getData().trim();
                    Log.d("scanResult: ", scanResult);

                    if(scanResult.contains("|")) {
                        String locationid[] = scanResult.split("\\|");
                        System.out.print(locationid[0]);
                        LocationID = locationid[0];
                        int length_LocationID = LocationID.length();
                        LocationName = scanResult.substring(length_LocationID+2,length_LocationID+11);

                        String sub_scanResult = scanResult.substring(length_LocationID + 2, scanResult.length());
                        String locationname[] = sub_scanResult.split("\\|");
                        LocationName1 = locationname[0];
                        int length_LocationName1 = LocationName1.length();

                        String sub_scanResult1 = sub_scanResult.substring(length_LocationName1 + 2, sub_scanResult.length());
                        String unitname[] = sub_scanResult1.split("\\|");
                        Unitname = unitname[0];
                        int length_UnitName = Unitname.length();

                        String sub_scanResult2 = sub_scanResult1.substring(length_UnitName + 2, sub_scanResult1.length());
                        String sitename[] = sub_scanResult2.split("\\|");
                        SiteName = sitename[0];
                        int length_SiteName = SiteName.length();

                        String sub_scanResult3 = sub_scanResult2.substring(length_SiteName + 2, sub_scanResult2.length());
                        String countyname[] = sub_scanResult3.split("\\|");
                        CountyName = countyname[0];
                        int length_CountyName = CountyName.length();

                        StateName = sub_scanResult3.substring(length_CountyName+2, sub_scanResult3.length());

                    }

                    if(LocationName.equals("Location1") || LocationName.equals("Location2") || LocationName.equals("Location3") || LocationName.equals("Location4") || LocationName.equals("Location5")){
                        UserDBHandler db = new UserDBHandler(QR_BarcodeScanActivity.this);
                        Log.d("Reading: ", "Reading all users..");
                        List<User> users = db.getAllUsers();
                        for (User user : users) {
                            String log = "Id: " + user.getId() + " ,UserID: " + user.get_id() + " ,Company: " + user.getCompany() + " ,CardID: " + user.getCardid();
                            Log.d("User: : ", log);
                        }
                        User lastUser = new User();
                        Integer last = users.size();
                        lastUser = db.getUser(last);
                        UserID = lastUser.get_id();

                        tsLong = getCurrentTimeStamp();
                        Current_Timestamp = tsLong.toString();
                        Current_Time = getCurrentLocalTime();

                        ScanDBHandler scandb = new ScanDBHandler(QR_BarcodeScanActivity.this);
                        List<Scan> scans = scandb.getAllScans();
                        int scans_number = scans.size();
                        if (scans_number > 0){
                            for (Scan scan : scans) {
                                String log = "Id: " + scan.getId() + " ,UserID: " + scan.getUserid()
                                        + " ,CurrentTime: " + scan.getCurrenttime() + " ,TimeStamp: " + scan.getTimestamp()
                                        + " ,LocationID: " + scan.getLocationid() + " ,Type: " + scan.getType();
                                Log.d("Scans: : ", log);
                            }
                            //last user finding
                            Scan lastScan = new Scan();
                            lastScan = scandb.getScan(scans_number);
                            current_Type = lastScan.getType();
                            visitID = String.valueOf(lastScan.getId()+1);
                            if(current_Type.equals("enter")){
                                Type = "exit";
                            }else{
                                Type = "enter";
                            }
                        }else{
                            visitID = "1";
                            if(Type.equals("enter")){
                                Type = "exit";
                            }else {
                                Type = "enter";
                            }
                        }

                        showAlertDialog("You have successfully scanned at " + LocationName1 + Unitname + SiteName + CountyName + StateName+" at "+ Current_Time);
                        barcodeScanned = true;

                        break;
                    }
                    else {
                        new AlertDialog.Builder(QR_BarcodeScanActivity.this)
                                .setTitle(getResources().getString(R.string.app_name))
                                .setMessage("Sorry QR code is not recognized.")
                                .setPositiveButton("OK", null).show();
                        barcodeScanned = true;

                        break;
                    }
                }
            }
        }
    };

    Camera.AutoFocusCallback autoFocusCB = new Camera.AutoFocusCallback() {
        public void onAutoFocus(boolean success, Camera camera) {
            autoFocusHandler.postDelayed(doAutoFocus, 1000);
        }
    };

    private void showAlertDialog(String message) {

        new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.app_name))
                .setCancelable(false)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        ScanDBHandler scan_db = new ScanDBHandler(QR_BarcodeScanActivity.this);
                        Log.d("Insert: ", "Inserting ..");
                        scan_db.addScan(new Scan(1, visitID, UserID, LocationID, LocationName, Current_Time, Current_Timestamp, Type, "", ""));
                        Log.d("Reading: ", "Reading all scans..");
                        List<Scan> scans = scan_db.getAllScans();
                        int scans_number = scans.size();
                        for (Scan scan : scans) {
                            String log = "Id: " + scan.getId() + "visitID: "+scan.getVisitid() + " ,UserID: " + scan.getUserid() + " ,LocationID: " + scan.getLocationid() + " ,CurrentTime: " + scan.getCurrenttime() + " ,TimeStamp: " + scan.getTimestamp() + "<type:>"+scan.getType();
                            Log.d("Scan: : ", log);
                        }
                        Scan lastScan = new Scan();
                        lastScan = scan_db.getScan(scans_number);
                        visitID = lastScan.getVisitid();
                        LocationID = lastScan.getLocationid();
                        Type = lastScan.getType();

                        ConnectivityManager conMgr =  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
                        {
                            NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                            if (netInfo == null) {
                                new AlertDialog.Builder(QR_BarcodeScanActivity.this)
                                        .setTitle(getResources().getString(R.string.app_name))
                                        .setMessage(getResources().getString(
                                                R.string.internet_error))
                                        .setPositiveButton("OK", null).show();
                                LocationName = "";
                            } else {
                                AsyncTS asyncT = new AsyncTS();
                                asyncT.execute();
                            }
                        }
                    }
                }).show();
    }

    private class AsyncTS extends AsyncTask<String , String, String> {

        protected String doInBackground(String... params) {
            try {
                URL url = new URL("http://ptracks.com/api/visits");

                HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                connection.setDoOutput(true);
                connection.setDoInput(true);
                connection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
                connection.setRequestMethod("POST");
                JSONObject jsonobj = new JSONObject();
                JSONObject visitsInfo = new JSONObject();
                visitsInfo.put("visitid", visitID);
                visitsInfo.put("locationid", LocationID);
                visitsInfo.put("timestamp", tsLong);
                visitsInfo.put("type",Type);
                jsonobj.put("userid", UserID);
                jsonobj.put("visits", visitsInfo);
                String s = jsonobj.toString();
                OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
                wr.write(jsonobj.toString());
                wr.flush();
                sb = new StringBuilder();
                int HttpResult = connection.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(connection.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();
                    System.out.println("" + sb.toString());
                } else {
                    System.out.println(connection.getResponseMessage());
                }
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());
            }
            return sb.toString();
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {

                JSONObject object = new JSONObject(result);
                Log.d("My App", object.toString());

                JSONArray json_array = object.getJSONArray("success");
                if(json_array.length() == 0){
                    Toast.makeText(getApplicationContext(),"Service response failed.",Toast.LENGTH_LONG).show();
                }else{
                    Intent intent = new Intent(QR_BarcodeScanActivity.this, ScanLogActivity.class);
                    startActivity(intent);
                    releaseCamera();
                    finish();
                }

            } catch (Throwable t) {
                Log.e("My App", "Could not parse malformed JSON: \"" + result + "\"");
            }
        }
    }

   public static String getCurrentLocalTime(){
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+1:00"));
        Date currentLocalTime = cal.getTime();
        DateFormat date = new SimpleDateFormat("dd/MM/yyyy HH:mm a");
        date.setTimeZone(TimeZone.getTimeZone("GMT+1:00"));
        String localTime = date.format(currentLocalTime);
        return localTime;
    }
    public static long getCurrentTimeStamp(){
        long ts = System.currentTimeMillis();
        Date localTime = new Date(ts);
        System.out.println("TimeStamp:" + localTime.getTime());
        return localTime.getTime();
    }
}
