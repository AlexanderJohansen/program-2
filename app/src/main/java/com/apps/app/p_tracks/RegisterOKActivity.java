package com.apps.app.p_tracks;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterOKActivity extends AppCompatActivity {

    Button btnRegisterOK;
    ImageButton btnMenu;
    TextView responseText;

    String UserID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_ok);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("P-TRACKS");
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.nav_scanlog) {
                    // Handle the scanlog  action
                    Intent intent = new Intent(RegisterOKActivity.this, ScanLogActivity.class);
                    startActivity(intent);
                    RegisterOKActivity.this.finish();
                } else if (id == R.id.nav_profile) {
                    // Handle the profile action
                    Intent intent = new Intent(RegisterOKActivity.this, RegisterActivity.class);
                    intent.putExtra("user_state","update_state");
                    startActivity(intent);
                    RegisterOKActivity.this.finish();
                } else if (id == R.id.nav_feedback) {
                    // Handle the feedback action
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(RegisterOKActivity.this);
                    alertDialog.setTitle("Feedback");
                    alertDialog.setMessage("Enter Feedback");

                    final EditText input = new EditText(RegisterOKActivity.this);
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT);
                    input.setLayoutParams(lp);
                    alertDialog.setView(input);
                    alertDialog.setIcon(R.drawable.feedback);

                    alertDialog.setPositiveButton("YES",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    String feedback;
                                    feedback = input.getText().toString();
                                    Toast.makeText(getApplicationContext(),
                                            feedback, Toast.LENGTH_SHORT).show();
                                }
                            });

                    alertDialog.setNegativeButton("NO",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });

                    alertDialog.show();
                }
                else if(id==R.id.nav_quit)
                {
                    // Handle the quit action
                    RegisterOKActivity.this.finish();
                }

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });


        btnRegisterOK = (Button) findViewById(R.id.RegisterOK);
        responseText = (TextView) findViewById(R.id.responseServe);

        Intent intent = getIntent();
        UserID = intent.getStringExtra("UserID");

        btnRegisterOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(RegisterOKActivity.this, ScanActivity.class);
                intent.putExtra("UserID", UserID);
                startActivity(intent);
                finish();
            }
        });
    }

}
