package com.apps.app.p_tracks;

/**
 * Created by Alexander on 6/15/2016.
 */
public class User {
    private int id;
    private String _id;
    private String __v;
    private String company;
    private String cardid;
    private String jobtitle;
    private String estatus;
    private String lastname;
    private String firstname;
    private int isactive;
    private int created;
    private int registered1;
    private int lastactive1;
    private Boolean isactive1;
    private String id1;

    public User(){

    }

    public User(int id, String _id, String company, String cardid, int isactive, String jobtitle, String estatus, String firstname, String lastname, int created){
        this.id = id;
        this._id = _id;
        this.company = company;
        this.cardid = cardid;
        this.isactive = isactive;
        this.jobtitle = jobtitle;
        this.estatus = estatus;
        this.firstname = firstname;
        this.lastname = lastname;
        this.created = created;
    }

    public void setId(int id){
        this.id = id;
    }

    public void set_id(String _id){
        this._id = _id;
    }

    public void setCompany(String company){
        this.company = company;
    }

    public void setCardid(String cardid){
        this.cardid = cardid;
    }

    public void setIsactive(int isactive){
        this.isactive = isactive;
    }

    public void setJobtitle(String jobtitle){
        this.jobtitle = jobtitle;
    }

    public void setEstatus(String estatus){
        this.estatus = estatus;
    }

    public void setFirstname(String firstname){
        this.firstname = firstname;
    }

    public void setLastname(String lastname){
        this.lastname = lastname;
    }

    public void setCreated(int created){
        this.created = created;
    }

    public int getId(){
        return id;
    }

    public String get_id(){
        return  _id;
    }

    public String getCompany(){
        return company;
    }

    public String getCardid(){
        return cardid;
    }

    public int getIsactive(){
        return isactive;
    }

    public String getJobtitle(){
        return jobtitle;
    }

    public String getEstatus(){
        return estatus;
    }

    public String getFirstname(){
        return firstname;
    }

    public String getLastname(){
        return lastname;
    }

    public int getCreated(){
        return created;
    }
}
