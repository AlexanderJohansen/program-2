package com.apps.app.p_tracks;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

public class ScanLogActivity extends AppCompatActivity {

    Button btnScan;
    ListView listScanLog;

    String[] time_array = new String[500];
    String[] location_array = new String[500];
    String[] type_array = new String[500];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_log);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("P-TRACKS");
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.nav_scanlog) {
                } else if (id == R.id.nav_profile) {
                    // Handle the profile action
                    Intent intent = new Intent(ScanLogActivity.this, RegisterActivity.class);
                    intent.putExtra("user_state","update_state");
                    startActivity(intent);
                    ScanLogActivity.this.finish();
                } else if (id == R.id.nav_feedback) {
                    // Handle the feedback action
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(ScanLogActivity.this);
                    alertDialog.setTitle("Feedback");
                    alertDialog.setMessage("Enter Feedback");

                    final EditText input = new EditText(ScanLogActivity.this);
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT);
                    input.setLayoutParams(lp);
                    alertDialog.setView(input);
                    alertDialog.setIcon(R.drawable.feedback);

                    alertDialog.setPositiveButton("YES",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    String feedback;
                                    feedback = input.getText().toString();
                                    Toast.makeText(getApplicationContext(),
                                            feedback, Toast.LENGTH_SHORT).show();
                                }
                            });

                    alertDialog.setNegativeButton("NO",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });

                    alertDialog.show();
                }
                else if(id==R.id.nav_quit)
                {
                    // Handle the quit action
                    ScanLogActivity.this.finish();
                }

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });

        btnScan = (Button) findViewById(R.id.Scan);

        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(ScanLogActivity.this, QR_BarcodeScanActivity.class);
                startActivity(intent);
                finish();

            }
        });

        listScanLog = (ListView) findViewById(R.id.scanlog);

        //getting Type
        ScanDBHandler scandb = new ScanDBHandler(ScanLogActivity.this);
        List<Scan> scans = scandb.getAllScans();
        //checking if there are users
        int scans_number = scans.size();
        int i = 0;
        if (scans_number > 0){//if the User has registered alreasy
            //finding whole scan log
            Scan scan = new Scan();
            for(i = 1;i <= scans_number;i ++){
                scan = scandb.getScan(i);
                time_array[i] = scan.getCurrenttime();
                location_array[i] = scan.getLocation();
                type_array[i] = scan.getType();
            }
        }else{
//            Toast.makeText(getApplicationContext(),"There isn't Scan Log now", Toast.LENGTH_LONG).show();
        }

        listScanLog.setAdapter(new location_itemadapter(this, time_array, location_array, type_array));
    }

}
