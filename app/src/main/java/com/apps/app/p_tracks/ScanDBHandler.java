package com.apps.app.p_tracks;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander on 6/16/2016.
 */
public class ScanDBHandler extends SQLiteOpenHelper {
    // Database Version
    private static final int DATABASE_VERSION = 3;
    // Database Name
    private static final String DATABASE_NAME = "scanInfo";
    // Contacts table name
    private static final String TABLE_SCAN = "scan";
    // Scan Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_VISITID = "visitid";
    private static final String KEY_USERID = "userid";
    private static final String KEY_LOCATIONID = "locationid";
    private static final String KEY_LOCATION = "location";
    private static final String KEY_CURRENTTIME = "currenttime";
    private static final String KEY_TIMESTAMP = "timestamp";
    private static final String KEY_TYPE = "type";
    private static final String KEY_SYNCDATETIME = "syncdatetime";
    private static final String KEY_SERVERID = "serverid";

    public ScanDBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_SCAN + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_VISITID + " TEXT," + KEY_USERID + " TEXT,"
                + KEY_LOCATIONID + " TEXT," + KEY_LOCATION + " TEXT," + KEY_CURRENTTIME + " TEXT," + KEY_TIMESTAMP + " TEXT,"
                + KEY_TYPE + " TEXT,"+ KEY_SYNCDATETIME + " TEXT,"
                + KEY_SERVERID + " TEXT" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCAN);
        // Creating tables again
        onCreate(db);
    }

    // Adding new Scan
    public void addScan(Scan scan) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_VISITID, scan.getVisitid());
        values.put(KEY_USERID, scan.getUserid());
        values.put(KEY_LOCATIONID, scan.getLocationid());
        values.put(KEY_LOCATION, scan.getLocation());
        values.put(KEY_CURRENTTIME, scan.getCurrenttime());
        values.put(KEY_TIMESTAMP, scan.getTimestamp());
        values.put(KEY_TYPE, scan.getType());
        values.put(KEY_SYNCDATETIME, scan.getSyncDatetime());
        values.put(KEY_SERVERID, scan.getServerID());
        //
        db.insert(TABLE_SCAN, null, values);
        db.close(); // Closing database connection
    }

    // Getting one Scan
    public Scan getScan(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_SCAN, new String[]{KEY_ID,
                        KEY_VISITID, KEY_USERID, KEY_LOCATIONID, KEY_LOCATION, KEY_CURRENTTIME, KEY_TIMESTAMP, KEY_TYPE, KEY_SYNCDATETIME, KEY_SERVERID}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        Scan contact = new Scan(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5),
                cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9));
        // return scan
        return contact;
    }

    // Getting All Scans
    public List<Scan> getAllScans() {
        List<Scan> scanList = new ArrayList<Scan>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_SCAN;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Scan scan = new Scan();
                scan.setId(Integer.parseInt(cursor.getString(0)));
                scan.setVisitid(cursor.getString(1));
                scan.setUserid(cursor.getString(2));
                scan.setLocationid(cursor.getString(3));
                scan.setLocation(cursor.getString(4));
                scan.setCurrenttime(cursor.getString(5));
                scan.setTimestamp(cursor.getString(6));
                scan.setType(cursor.getString(7));
                scan.setSyncDatetime(cursor.getString(8));
                scan.setServerID(cursor.getString(9));
                // Adding contact to list
                scanList.add(scan);
            } while (cursor.moveToNext());
        }
        // return contact list
        return scanList;
    }

    // Getting Scans Count
    public int getScansCount() {
        String countQuery = "SELECT * FROM " + TABLE_SCAN;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();
        // return count
        return cursor.getCount();
    }

    // Updating a Scan
    public int updateScan(Scan scan) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_VISITID, scan.getVisitid());
        values.put(KEY_USERID, scan.getUserid());
        values.put(KEY_LOCATIONID, scan.getLocationid());
        values.put(KEY_LOCATION, scan.getLocationid());
        values.put(KEY_CURRENTTIME, scan.getCurrenttime());
        values.put(KEY_TIMESTAMP, scan.getTimestamp());
        values.put(KEY_TYPE, scan.getType());
        values.put(KEY_SYNCDATETIME, scan.getSyncDatetime());
        values.put(KEY_SERVERID, scan.getServerID());

        // updating row
        return db.update(TABLE_SCAN, values, KEY_ID + " = ?",
                new String[]{String.valueOf(scan.getId())});
    }

    // Deleting a Scan
    public void deleteScan(Scan scan) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SCAN, KEY_ID + " = ?",
                new String[]{String.valueOf(scan.getId())});
        db.close();
    }
}
